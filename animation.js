function start(){
    //cria canvas
    var cnv = document.getElementById("cnv");
    var ctx = cnv.getContext("2d");

    ctx.canvas.height = 600;
    ctx.canvas.width = 900;
    ctx.transform(1, 0, 0, -1, 0, cnv.height);

    //cria objeto bola
    var bola = {
        x: 0,
        y: 0,
        raio: 30,
    };

    var walle = {
        x: 0,
        y: 0,
        height: 170,
        width: 170,
    };

    var i=0;
        function loop(){
            update();
            desenha();
        }

        //função para desenhar os elementos do canvas
        function desenha(){
            //fundo:
            var campo = new Image();
            campo.src = 'campo.jpg'
            ctx.drawImage(campo, 0, 0, cnv.width, cnv.height);

            //bola:
            var imagem_bola = new Image();
            imagem_bola.src = 'bola.png';
            ctx.drawImage(imagem_bola, bola.x, bola.y, bola.raio*2, bola.raio*2);

            //robot:
            var imagem_walle = new Image();
            imagem_walle.src = 'robot.png';
            ctx.drawImage(imagem_walle, walle.x -20, walle.y -30, walle.height, walle.width);

            //texto:
            ctx.fillStyle = "white";
            ctx.font = "20px Arial";
        }

        //função que faz a bola se movimentar
        function update(){
        //bola andando nos vetores xy
        //condicao para bola parar
            if(i<tempCont-n){
                bola.x = (posBolaX[i]*100);
              	bola.y = (posBolaY[i]*100);
                walle.x = (posRoboX[i]*100);
                walle.y = (posRoboY[i]*100);
                i++;

            }
        }
        setInterval(loop, 20);
}
